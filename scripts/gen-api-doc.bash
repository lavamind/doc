#!/usr/bin/env bash

set -e

OUTPUT=$PWD/output
PUBLIC=$PWD/public

if [[ ! -d "${OUTPUT}" ]] ; then
    mkdir "$OUTPUT"
fi

if [[ ! -d "${OUTPUT}/tor" ]] ; then
    git clone https://gitlab.torproject.org/tpo/core/tor.git "${OUTPUT}/tor"

    cd "$OUTPUT/tor"
    sh autogen.sh
    ./configure --enable-doxygen
    make doxygen
fi


if [[ ! -d "${OUTPUT}/arti" ]] ; then
    git clone https://gitlab.torproject.org/tpo/core/arti.git "${OUTPUT}/arti"

    cd "$OUTPUT/arti"
    cargo doc --no-deps --all-features
    mv "${OUTPUT}/arti/target/doc" "${OUTPUT}/arti/documentation"
    cargo doc --no-deps --all-features --document-private-items
    mv "${OUTPUT}/arti/target/doc" "${OUTPUT}/arti/documentation/private"

    echo '<meta http-equiv=refresh content="0; url=arti_client/index.html">' > "${OUTPUT}/arti/documentation/index.html"
    echo '<meta http-equiv=refresh content="0; url=arti_client/index.html">' > "${OUTPUT}/arti/documentation/private/index.html"
fi

if [[ ! -d "${PUBLIC}" ]] ; then
    mkdir "$PUBLIC"
fi

mv "${OUTPUT}/tor/doc/doxygen/html" "${PUBLIC}/tor"
mv "${OUTPUT}/arti/documentation" "${PUBLIC}/rust"

# Copy files from version control into $PUBLIC
for fname in $(git ls-tree -r --name-only HEAD static/); do
    target_fname="${PUBLIC}/${fname#static/}"
    mkdir -p "$(dirname $target_fname)"
    cp "$fname" "$target_fname"
done
